#!/usr/bin/env python 
"""
attribute-labels - show/hide attribute values as text objects (labels)

Copyright (C) 2012 ~suv, suv-sf@users.sourceforge.net


Idea:
> We use the fantastic extension from Sozi
> http://sozi.baierouge.fr/wiki/doku.php, which turns Inkscape into
> viable Prezi http://prezi.com/ open source alternative for better
> Powerpoints in SVG!
> 
> To become productive making Prezi presentations, we need an easy way
> to show labels of all frames, Sozi uses rectangles, on the canvas .
> Opening object properties of each object is way too slow.
> 
> Is there a way to plot the Object Label on the canvas? For example at
> the bottom right corner of each selected object ?
<http://www.inkscapeforum.com/viewtopic.php?f=22&t=13205>


This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

# system library

# local library
import inkex
import simplestyle
import simpletransform

try:
    inkex.localize()
except:
    #inkex.errormsg("inkex.localize() failed.\n")
    import gettext
    _ = gettext.gettext

class AttributeLabels(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.OptionParser.add_option("--attribute",
            action="store", type="string",
            dest="attribute", default="inkscape:label",
            help="Attribute to be shown/hidden")
        self.OptionParser.add_option("--action",
            action="store", type="string",
            dest="action", default="update",
            help="Label action")
        self.OptionParser.add_option("--fontsize",
            action="store", type="float",
            dest="fontsize", default=20.0,
            help="Font size")
        self.OptionParser.add_option("--offset",
            action="store", type="float",
            dest="offset", default=5.0,
            help="Offset")
        self.OptionParser.add_option("--fontcolor",
            action="store", type="string",
            dest="fontcolor", default="#090909",
            help="Font color")
        self.OptionParser.add_option("--tab",
            action="store", type="string",
            dest="tab")

    def getLabelAnchor(self, node):
        """
        Get approx. geometric bounding box of selection and return upper left point
        """
        if node.tag == inkex.addNS('rect','svg'):
            x = node.get('x')
            y = float(node.get('y')) - self.options.offset
        elif node.tag == inkex.addNS('path','svg'):
            bbox = simpletransform.computeBBox([node])
            x = bbox[0]
            y = float(bbox[2]) - self.options.offset
        else:
            inkex.errormsg(_("Object type not supported"))
            x = y = 0
        return(x, y)

    def updateLabelPos(self, node, label):
        """
        Update label position (text anchor)
        """
        pos = self.getLabelAnchor(node)
        label.set('x', str(pos[0]))
        label.set('y', str(pos[1]))

    def updateLabelStyle(self, label):
        """
        Update label style to current options
        """
        style = {'font-size': self.options.fontsize, 'fill': self.options.fontcolor, 'display': 'true'}
        label.set('style', simplestyle.formatStyle(style))

    def updateLabelText(self, label, name, text):
        """
        Update label content (attribute name & text)
        """
        label.text = "%s: %s" % (name, text)

    def updateLabel(self, node, label, attr):
        """
        Update label content, style and position
        """
        text = self.getAttrVal(node, attr)
        self.updateLabelText(label, attr[-1], text)
        self.updateLabelStyle(label)
        self.updateLabelPos(node, label)

    def showLabel(self, label):
        """
        Show label if hidden 
        """
        oldstyle = label.get('style')
        if 'display:none' in oldstyle:
            newstyle = oldstyle.replace('display:none','display:true')
        else:
            newstyle = "%s;display:true" % oldstyle
        label.set('style', newstyle)

    def hideLabel(self, label):
        """
        Hide label if visible
        """
        oldstyle = label.get('style')
        if 'display:true' in oldstyle:
            newstyle = oldstyle.replace('display:true','display:none')
        else:
            newstyle = "%s;display:none" % oldstyle
        label.set('style', newstyle)

    def delLabel(self, label):
        """
        Delete label if present
        """
        label.getparent().remove(label)

    def writeLabel(self, parent, node, name, text):
        """
        Create label text object, take style and offset from options
        """
        label = inkex.etree.SubElement(parent, inkex.addNS('text','svg'))
        label.set(inkex.addNS('label','inkscape'), "%s_%s" % (node.get('id'), name))
        label.set(inkex.addNS('insensitive','sodipodi'), "true")

        self.updateLabelStyle(label)
        self.updateLabelPos(node, label)
        self.updateLabelText(label, name, text)

        parent.insert(parent.index(node)+1, label)
        return label

    def getAttrVal(self, node, attr):
        """
        Get value for attr of node and return string
        """
        if attr[0] == "svg":
            text = node.get(attr[-1])
        else:
            text = node.get(inkex.addNS(attr[1], attr[0]))
        return text

    def addAttrLabel(self, node, attr):
        """
        Create object with value for 'attr' if not present
        """
        parent = node.getparent()
        attr_name = attr[-1]
        attr_val = self.getAttrVal(node, attr)
        label = self.writeLabel(parent, node, attr_name, attr_val)

    def checkAttrLabel(self, node, attr):
        """
        Check presence and visibility of attr for node, toggle visibilty or add new label if missing
        """
        path = '//*[@%s="%s_%s"]' % ("inkscape:label", node.get('id'), attr[-1])
        #inkex.debug("xpath expression: %s" % path)
        label_l = self.document.xpath(path, namespaces=inkex.NSS)
        #inkex.debug("objects with matching labels: %s" % str(label_l))
        if len(label_l) == 0:
            if self.options.action != "delete" and self.options.action != "update":
                self.addAttrLabel(node, attr)
        elif len(label_l) == 1:
            label = label_l[0]
            if self.options.action == "togglevis":
                if 'display:none' in label.get('style'):
                    self.updateLabel(node, label, attr)
                    self.showLabel(label)
                else:
                    self.hideLabel(label)
            elif self.options.action == "update": 
                self.updateLabel(node, label, attr)
            elif self.options.action == "delete":
                self.delLabel(label)
        elif self.options.action == "delete":
            for label in label_l: 
                self.delLabel(label)
        else:
            inkex.errormsg(_("Attribute labels: Error: more than one label for atttribute %s of id %s found" 
                % (attr[-1], node.get('id'))) )

    def effect(self):
        '''
        Effect: loop through selection and process each child individually
        '''
        attr = self.options.attribute.split(":")
        for node in self.selected.itervalues():
            status = self.checkAttrLabel(node, attr)

if __name__ == '__main__':
    e = AttributeLabels()
    e.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
